(ns table-filter.cache
  (:require
    [clojure.math :as math]
    [reagent.core :as r]
    [promesa.core :as p]
    [table-filter.util :refer [filter-kv]]))

(defn expire-t [output]
  (:expire-t (meta output)))

(defn not-expired? [this output]
  (let [{:keys [now]} this]
    (<= (now) (expire-t output))))

(defn get-cache [this input]
  (let [{:keys [cache]} this]
    (when-let [output (get @cache input)]
      (when (not-expired? this output)
        output))))

(defn add-cache! [this input output]
  (let [{:keys [cache now expire-ms]} this]
    (swap! cache assoc input (with-meta output {:expire-t (+ (now) expire-ms)}))))

(defn gc-cache! [this]
  (let [{:keys [cache keep-recent]} this]
    (let [recent? (->> @cache
                       (sort-by (comp expire-t second) >=)
                       (take keep-recent)
                       (map first)
                       (set))]
      (swap! cache filter-kv #(and (recent? %1) (not-expired? this %2))))))

(defn fill-defaults [this]
  (-> this
      (update :now #(or % js/Date.now))
      (update :expire-ms #(or % 5000))
      (update :keep-recent #(or % 3))))

(defn cached-fetch! [this input] 
  (let [{:keys [fetch now] :as this} (fill-defaults this)]
    (gc-cache! this)
    (if-let [output (get-cache this input)]
      (p/resolved output)
      (p/let [output (fetch input)]
        (add-cache! this input output)
        output))))

(defn render-cache [this]
  (r/with-let [{:keys [now]} (fill-defaults this)
               *now (r/atom (now))
               i (js/setInterval #(reset! *now (now)) 500)]
    (let [{:keys [cache]} this]
      [:div.flex.ml-4
       (doall
         (for [[k v] (->> @cache (sort-by (comp expire-t second) >=))
               :let [t (-> (expire-t v) (- @*now) (/ 1000) (max 0) math/ceil)]
               :when (< 0 t)]
           [:ruby.m-2 {:key k} k [:rt t "s"]]))])
    (finally
      (js/clearInterval i))))
