(ns table-filter.step4
  (:require
    [clojure.string :as str]
    [reagent.core :as r]
    [promesa.core :as p]
    [promesa.exec :as px]
    [goog.functions :refer [debounce]]
    [table-filter.cache :refer [cached-fetch! render-cache]]
    [table-filter.util :refer [fetch-json! indexed non-empty-str]]))

(defonce cache
  {:fetch #(fetch-json! (str "/search?term=" %))
   :cache (r/atom {})
   :keep-recent 3
   :expire-ms 5000})

(defonce ui
  (r/atom {:search ""
           :match ""
           :rows nil
           :loading? false}))

(defn submit-search! []
  (when-let [search (non-empty-str (:search @ui))]
    (p/let [loading (px/schedule! 500 #(swap! ui assoc :loading? true))
            rows (cached-fetch! cache search)]
       (p/cancel! loading)
       (swap! ui assoc :rows rows :loading? false :match search))))

(def debounced-submit-search!
  (debounce submit-search! 500))

(defn update-search! [e]
  (swap! ui assoc :search (.. e -target -value))
  (debounced-submit-search!))

(defn table-search []
  [:div.mb-4.flex
    [:input.border.p-2
     {:type "text"
      :value (:search @ui)
      :on-change update-search!}]
    [render-cache cache]])

(defn highlight-match [text match]
  (or
    (when-let [match (non-empty-str match)]
      (when-let [a (str/index-of (str/lower-case text)
                                 (str/lower-case match))]
        (let [b (+ a (count match))]
          [:<>
            (subs text 0 a)
            [:span.bg-yellow-200.font-bold (subs text a b)]
            (subs text b)])))
    text))

(defn table-results []
  (let [td-class "p-2 text-left whitespace-nowrap"
        {:keys [rows match]} @ui
        cols [:name :phone :address]]
    (cond
      (:loading? @ui) "Loading…"
      (nil? rows) nil
      (empty? rows) "No results."
      :else
      [:table
       [:thead
        [:tr
         (for [col cols]
           [:th {:key col :class td-class} (str/capitalize (name col))])]]
       [:tbody
        (for [[i row] (indexed rows)]
          [:tr {:key i}
           (for [col cols]
             [:td {:key col :class td-class}
              (cond-> (get row col)
                (= col :name) (highlight-match match))])])]])))

(defn root []
  [:div
    [table-search]
    [table-results]])
