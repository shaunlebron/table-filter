(ns table-filter.util
  (:require
    [clojure.string :as str]
    [promesa.core :as p]))

(defn filter-kv [m f]
  (reduce-kv
    #(cond-> %1 (f %2 %3) (assoc %2 %3))
    {} m))

(defn indexed [coll]
  (map-indexed vector coll))

(defn fetch-json! [url]
  (p/let [resp (js/fetch url)
          json (.json resp)]
    (js->clj json :keywordize-keys true)))

(defn non-empty-str [s]
  (let [text (str/trim s)]
    (when (not= "" text)
      text)))
