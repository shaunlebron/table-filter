(ns table-filter.core
  (:require
    [table-filter.step1 :as step1]
    [table-filter.step2 :as step2]
    [table-filter.step3 :as step3]
    [table-filter.step4 :as step4]
    [table-filter.step5 :as step5]
    [reagent.dom :as d]))

(defn root []
  [:div.flex.flex-col
   (for [[title step-root] [["#1 Filter" step1/root]
                            ["#2 Loading Indicator" step2/root]
                            ["#3 Filter-as-you-type" step3/root]
                            ["#4 Match highlight" step4/root]
                            ["#5 Multi-word match" step5/root]]]
     [:section.w-screen.p-16.border-b
       {:key title}
       [:h1.text-xl.font-bold.mb-16 title]
       [step-root]])])

(defn mount-root []
  (d/render [root] (.getElementById js/document "app")))

(defn ^:export init! []
  (mount-root))
