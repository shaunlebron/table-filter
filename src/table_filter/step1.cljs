(ns table-filter.step1
  (:require
    [clojure.string :as str]
    [reagent.core :as r]
    [promesa.core :as p]
    [table-filter.cache :refer [cached-fetch! render-cache]]
    [table-filter.util :refer [fetch-json! indexed non-empty-str]]))

(defonce cache
  {:fetch #(fetch-json! (str "/search?term=" %))
   :cache (r/atom {})
   :keep-recent 3
   :expire-ms 5000})

(defonce ui
  (r/atom {:search ""
           :rows nil}))

(defn submit-search! [e]
  (.preventDefault e)
  (when-let [search (non-empty-str (:search @ui))]
    (p/let [rows (cached-fetch! cache search)]
       (swap! ui assoc :rows rows))))

(defn update-search! [e]
  (swap! ui assoc :search (.. e -target -value)))

(defn table-search []
  [:form.mb-4.flex
    [:input.border.p-2.mr-2
     {:type "text"
      :value (:search @ui)
      :on-change update-search!}]
    [:button.border.p-2
     {:type "submit"
      :on-click submit-search!}
     "Search"]
    [render-cache cache]])

(defn table-results []
  (let [td-class "p-2 text-left whitespace-nowrap"
        {:keys [rows]} @ui
        cols [:name :phone :address]]
    (cond
      (nil? rows) nil
      (empty? rows) "No results."
      :else
      [:table
       [:thead
        [:tr
         (for [col cols]
           [:th {:key col :class td-class} (str/capitalize (name col))])]]
       [:tbody
        (for [[i row] (indexed rows)]
          [:tr {:key i}
           (for [col cols]
             [:td {:key col :class td-class} (get row col)])])]])))

(defn root []
  [:div
    [table-search]
    [table-results]])
