(ns table-filter.step5
  (:require
    [clojure.string :as str]
    [reagent.core :as r]
    [promesa.core :as p]
    [promesa.exec :as px]
    [goog.functions :refer [debounce]]
    [table-filter.cache :refer [cached-fetch! render-cache]]
    [table-filter.util :refer [fetch-json! indexed non-empty-str]]))

(defonce cache
  {:fetch #(fetch-json! (str "/search?term=" %))
   :cache (r/atom {})
   :keep-recent 3
   :expire-ms 5000})

(defonce ui
  (r/atom {:search ""
           :matches nil
           :rows nil
           :loading? false}))

(defn search-next! [search]
  (p/let [loading (px/schedule! 500 #(swap! ui assoc :loading? true))
          rows (cached-fetch! cache search)]
     (p/cancel! loading)
     (swap! ui assoc :loading? false)
     rows))

(defn matching-row? [row matches]
  (when (seq matches)
    (let [text (str/lower-case (:name row))]
      (->> matches
           (map str/lower-case)
           (every? #(str/includes? text %))))))

(defn collate-rows [rows next-rows matches]
  (->> (concat rows next-rows)
       (sort-by :name)
       (dedupe)
       (filterv #(matching-row? % matches))))

(defn get-matches []
  (when-let [search (non-empty-str (:search @ui))]
    (seq (str/split search #"\s+"))))

(defn submit-search! []
  (when-let [matches (get-matches)]
    (p/let [rows (search-next! (first matches))]
      (swap! ui assoc :matches matches :rows [])
      (swap! ui update :rows collate-rows rows matches)
      (p/doseq [search (rest matches)] 
        (p/delay 100)
        (p/let [rows (search-next! search)]
          (swap! ui update :rows collate-rows rows matches))))))

(def debounced-submit-search!
  (debounce submit-search! 500))

(defn update-search! [e]
  (swap! ui assoc :search (.. e -target -value))
  (debounced-submit-search! e))

(defn table-search []
  [:div
    [:div.mb-4.flex
      [:input.border.p-2
       {:type "text"
        :value (:search @ui)
        :on-change update-search!}]
      [render-cache cache]]
    (when (:loading? @ui)
      "Loading…")])

(defn highlight-cuts [text matches]
  (let [pairs (for [match matches
                    :let [i (str/index-of (str/lower-case text)
                                          (str/lower-case match))]
                    :when i]
                [i (+ i (count match))])
        all (flatten (sort-by first pairs))]
    `[0 ~@all ~(count text)]))

(defn highlight-matches [text matches]
  (when (seq matches)
    (for [[i [a b]] (->> (highlight-cuts text matches)
                         (partition 2 1)
                         (indexed))
          :let [subtext (subs text a b)]]
      (if (even? i)
        subtext
        [:span.bg-yellow-200.font-bold {:key i} subtext]))))

(defn table-results []
  (let [td-class "p-2 text-left whitespace-nowrap"
        {:keys [rows matches]} @ui
        cols [:name :phone :address]]
    (cond
      (nil? rows) nil
      (empty? rows) "No results."
      :else
      [:table
       [:thead
        [:tr
         (for [col cols]
           [:th {:key col :class td-class} (str/capitalize (name col))])]]
       [:tbody
        (for [[i row] (indexed rows)]
          [:tr {:key i}
           (for [col cols]
             [:td {:key col :class td-class}
              (cond-> (get row col)
                (= col :name) (highlight-matches matches))])])]])))

(defn root []
  [:div
    [table-search]
    [table-results]])
